﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using TheMerchandiserAPI.Helpers;

namespace TheMerchandiserAPI
{
    public class WebServer
    {
        private IDisposable _webapp;

        public void Start()
        {
            var port = MerchandiserFunctions.ReadOptionFromIni("APIPort", "8080");
            _webapp = WebApp.Start<Startup>("http://*:" + port);
            Console.WriteLine("API Service Started on http://" + CommonFunctions.GetLocalIP() + ":" + port);
        }

        public void Stop()
        {
            _webapp?.Dispose();
        }
    }
}