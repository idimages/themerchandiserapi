﻿using Owin;
using System.Linq;
using System.Web.Http;

namespace TheMerchandiserAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration();

            // Enable attribute-based routing for the application
            config.MapHttpAttributeRoutes();

            // Set up a default route template for the application
            // The route will include the word "api" followed by the name of the controller
            // and an optional id parameter
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Get the first media type with a value of "application/xml" from the list of
            // supported media types for the XML formatter. This ensures that the application will always return
            // JSON responses.
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");

            // If the media type was found, remove it from the list of supported media types
            if (appXmlType != null)
            {
                config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
            }

            // Use Web API with the configured routes and formatters
            app.UseWebApi(config);
        }
    }
}

