﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace TheMerchandiserAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            StartService();
        }

        static void StartService()
        {

            //using (EventLog eventLog = new EventLog("Application"))
            //{
            //    eventLog.Source = "MerchandiserAPI";
            //    eventLog.WriteEntry("Merchandiser API Started", EventLogEntryType.Information, 101);
            //}

            HostFactory.Run(x =>
            {
                x.Service<WebServer>(s =>
                {
                    s.ConstructUsing(name => new WebServer());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });

                x.RunAsLocalSystem();
                x.SetDescription("Service that allows API connections to the Merchandiser Databases and settings.");
                x.SetDisplayName("Shelf2Cart Merchandiser API");
                x.SetServiceName("MerchandiserAPI");

            });
        }
    }
}
