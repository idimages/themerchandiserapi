﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TheMerchandiserAPI.Models;
using TheMerchandiserAPI.Services;

namespace TheMerchandiserAPI.Helpers
{
    public static class CommonFunctions
    {
        /// <summary>
        /// Retrieves a record from a database based on a given product look-up code (PLU).
        /// </summary>
        /// <typeparam name="T">The type of object to return the record as.</typeparam>
        /// <param name="plu">The PLU to use for the search.</param>
        /// <returns>An object of type T containing the record data.</returns>
        public static async Task<T> GetPLU<T>(string plu) where T : new()
        {
            T returntype = new T();
            
            DataTable dt;

            if (typeof(T) != typeof(PrintRecord))
            {
                var _residentDB = new MSAccessConnector(@"C:\Program Files\MM_Label\database\nutrition.mdb");
                dt = await _residentDB.GetDataTable("SELECT * FROM [tblproducts] WHERE PLU='" + plu + "';");
            }
            else
            {
                var _loggingDB = new MSAccessConnector(@"C:\Program Files\MM_Label\system\printlog.mdb");
                dt = await _loggingDB.GetDataTable("SELECT * FROM [PrintHistory] WHERE PLU='" + plu + "';");
            }



            if (dt.Rows.Count > 0)
            {
                DataRow record = dt.Rows[0];

                foreach (DataColumn field in dt.Columns)
                {
                    string _columnName = field.ColumnName.ToString().Replace(" ", "");

                    if (!DBNull.Value.Equals(record[field]))
                    {
                        returntype.GetType().GetProperty(_columnName).SetValue(returntype, record[field], null);
                    }
                    else
                    {
                        returntype.GetType().GetProperty(_columnName).SetValue(returntype, null, null);
                    }
                }

            }

            return returntype;
        }

        /// <summary>
        /// Searches for products in a database based on a given search term.
        /// </summary>
        /// <param name="search">The search term to use for the search.</param>
        /// <returns>A data table containing the search results.</returns>
        public static async Task<DataTable> Search(string search)
        {
            // Connect to a database
            var _residentDB = new MSAccessConnector(@"C:\Program Files\MM_Label\database\nutrition.mdb");

            // Declare a data table to store the search results
            DataTable _searchDataTable;

            // Attempt to parse the search term as an integer
            int numericplu;
            if (int.TryParse(search, out numericplu))
            {
                // If the search term is an integer, search the PLU field in the database
                _searchDataTable = await _residentDB.GetDataTable("SELECT * FROM [tblproducts] WHERE PLU='" + numericplu + "';");
            }
            else
            {
                // If the search term is not an integer, search the Description 1 and Description 2 fields in the database
                _searchDataTable = await _residentDB.GetDataTable("SELECT * FROM [tblproducts] WHERE [Description 1] + ' ' + [Description 2] LIKE '%" + search + "%';");
            }

            // Set the table name and return the search results
            _searchDataTable.TableName = "SearchResults";
            return _searchDataTable;
        }

        /// <summary>
        /// Retrieves the local IP address of the current machine.
        /// </summary>
        /// <returns>The local IP address, or "0.0.0.0" if no IPv4 address is found.</returns>
        public static string GetLocalIP()
        {
            // Get host entry for the current machine
            var host = Dns.GetHostEntry(Dns.GetHostName());

            // Set default value for local IP address
            string ipaddress = "0.0.0.0";

            // Iterate through the list of addresses
            foreach (var ip in host.AddressList)
            {
                // If the address is an IPv4 address, set it as the local IP
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipaddress = ip.ToString();
                    // Break out of the loop since we have found the local IP
                    break;
                }
            }

            // Return the local IP address
            return ipaddress;
        }

    }
}
