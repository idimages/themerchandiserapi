﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMerchandiserAPI.Models;
using TheMerchandiserAPI.Services;

namespace TheMerchandiserAPI.Helpers
{
    public static class MerchandiserFunctions
    {
        /// <summary>
        /// Reads a value from an INI file in System/Options. If the file does not exist, it is created with the specified default value.
        /// </summary>
        /// <param name="iniFile">The name of the INI file to read from.</param>
        /// <param name="defaultvalue">The default value to use if the INI file does not exist.</param>
        /// <returns>The value read from the INI file, or the default value if the file did not exist.</returns>
        public static string ReadOptionFromIni(string iniFile, string defaultvalue = "")
        {
            string iniPath = @"C:\Program Files\MM_Label\System\Options\" + iniFile + ".ini";
            
            if (!File.Exists(iniPath))
            {
                using (StreamWriter sw = File.CreateText(iniPath))
                {
                    sw.WriteLine(defaultvalue);
                }
            }

            return File.ReadLines(iniPath).First();
        }

        /// <summary>
        /// Writes a value to an INI file System/Options, creating the file if it does not exist.
        /// </summary>
        /// <param name="iniFile">The name of the INI file to write to.</param>
        /// <param name="valuetoset">The value to write to the INI file.</param>
        /// <returns>The value written to the INI file.</returns>
        public static string SetOptionToIni(string iniFile, string valuetoset)
        {
            string iniPath = @"C:\Program Files\MM_Label\System\Options\" + iniFile + ".ini";

            using (StreamWriter sw = File.CreateText(iniPath))
            {
                sw.WriteLine(valuetoset);
            }

            return File.ReadLines(iniPath).First();
        }

        public static double PriceToDouble(string price)
        {
            return double.Parse(price);
        }

        public static string FormatCheck(string formatName)
        {
            return formatName += File.Exists(@"C:\Program Files\MM_Label\Labels\" + formatName + ".nlbl") ? ".nlbl" : ".lbl";
        }

        public async static Task<string> GeneratePreviewImages(int plu)
        {
            var NiceLabel2019 = new LabelSoftwareSDK();

            var printproduct = await CommonFunctions.GetPLU<LabelVariables>(plu.ToString());
            
            var Formats = new DirectoryInfo(@"C:\Program Files\MM_Label\Labels").GetFiles().Select(o => Path.GetFileNameWithoutExtension(o.Name)).ToArray();
            var formatlist = Formats.Distinct().ToArray();

            foreach (var format in formatlist)
            {
                try
                {
                    NiceLabel2019.LabelPath = @"C:\Program Files\MM_Label\Labels\" + MerchandiserFunctions.FormatCheck(format);
                    NiceLabel2019.SetAllLabelVariables(printproduct);

                    byte[] PreviewImage = NiceLabel2019.GeneratePreviewImage();

                    File.WriteAllBytes(@"C:\Program Files\MM_Label\Images\TestImages\" + plu + format + ".png", PreviewImage);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                
            }

            return "Test images saved";
        }

    }
}
