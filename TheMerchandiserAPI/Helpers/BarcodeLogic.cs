﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMerchandiserAPI.Helpers
{
    public class BarcodeLogic
    {
        public readonly int[][] chkDigitArrays =
        {
                new int[] { 0, 2, 4, 6, 8, 9, 1, 3, 5, 7 },
                new int[] { 0, 5, 9, 4, 8, 3, 7, 2, 6, 1 },
                new int[] { 0, 5, 1, 6, 2, 7, 3, 8, 4, 9 },
                new int[] { 0, 9, 7, 5, 3, 1, 8, 6, 4, 2 },
                new int[] { 0, 3, 6, 9, 2, 5, 8, 1, 4, 7 }
        };

        public int Factor2(int myDigit) { return chkDigitArrays[0][myDigit]; }
        public int Factor5Minus(int myDigit) { return chkDigitArrays[1][myDigit]; }
        public int Factor5Plus(int myDigit) { return chkDigitArrays[2][myDigit]; }
        public int GetCheckDigit(int myDigit) { return chkDigitArrays[3][myDigit]; }

        public string PaddedBarcode(string barcode)
        {
            return ("000000" + barcode).Substring(("000000" + barcode).Length - 6, 6);
        }

        public string BarcodePaddedPrice(string price)
        {
            return "00000" + price.Replace(".", string.Empty).Replace("$", string.Empty);
        }

        public string BarcodePriceVal(string barcodePaddedPrice, int priceLength)
        {
            return barcodePaddedPrice.Substring(barcodePaddedPrice.Length - priceLength, priceLength);
        }

        public string FormatTOT1Barcode(string Barcode, string barcodepriceval) 
		{
            return Barcode == "0" ? "" : Barcode + barcodepriceval;
        }
        
        public string FormatTOT2Barcode(string Barcode, string barcodepriceval)
        {

           int _priceCheck1 = 0;

            try
            {
                _priceCheck1 += chkDigitArrays[0][int.Parse(barcodepriceval[0].ToString())];
                _priceCheck1 += chkDigitArrays[0][int.Parse(barcodepriceval[1].ToString())];
                _priceCheck1 += chkDigitArrays[4][int.Parse(barcodepriceval[2].ToString())];
                _priceCheck1 += chkDigitArrays[1][int.Parse(barcodepriceval[3].ToString())];
                _priceCheck1 *= 3;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            string PriceCheck = _priceCheck1.ToString();
            PriceCheck = PriceCheck.Substring(PriceCheck.Length - 1, 1);

            return Barcode == "0" ? "" : Barcode + PriceCheck + barcodepriceval;
        }

        public string FormatEAN13Barcode(string barcode, string barcodePrice)
        {
            int _runningSum = 0;
            _runningSum += Factor5Plus(int.Parse(barcodePrice[0].ToString()));
            _runningSum += Factor2(int.Parse(barcodePrice[1].ToString()));
            _runningSum += Factor5Minus(int.Parse(barcodePrice[2].ToString()));
            _runningSum += Factor5Plus(int.Parse(barcodePrice[3].ToString()));
            _runningSum += Factor2(int.Parse(barcodePrice[4].ToString()));

            int _checkDigit = ((_runningSum % 10) != 0) ? 10 - (_runningSum % 10) : 0;  

            return barcode + GetCheckDigit(_checkDigit) + barcodePrice;
        }
    }
}
