﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using TheMerchandiserAPI.Helpers;
using TheMerchandiserAPI.Models;
using TheMerchandiserAPI.Services;
using Newtonsoft.Json;
using System.IO;

namespace TheMerchandiserAPI.Controllers
{
    [RoutePrefix("api")]
    public class ProductController : ApiController
    {
        [Route("getproducts/{searchid}")][HttpGet]
        public async Task<DataTable> Get(string searchid)
        {
            return await CommonFunctions.Search(searchid);
        }

        [Route("GetLabelPreview/{plu}")][HttpGet]
        public async Task<string> GetImage(string plu)
        {
            var NiceLabel2019 = new LabelSoftwareSDK();
            var _labelPLU = await CommonFunctions.GetPLU<LabelVariables>(plu);

            var _labelfile = Path.GetFileNameWithoutExtension(_labelPLU.LblName);

            NiceLabel2019.LabelPath = @"C:\Program Files\MM_Label\Labels\" + MerchandiserFunctions.FormatCheck(_labelfile);
            
            NiceLabel2019.SetAllLabelVariables(_labelPLU);
            byte[] PreviewImage = NiceLabel2019.GeneratePreviewImage();

            File.WriteAllBytes(@"C:\Program Files\MM_Label\Images\preview.png", PreviewImage);

            //return "test image saved";
            return Convert.ToBase64String(PreviewImage);

        }

        [Route("PrintPLU/{plu}")][HttpPost]
        public async Task<string> PrintPLU(int plu)
        {
            Console.WriteLine(plu.ToString());

            var NiceLabel2019 = new LabelSoftwareSDK();
            var printRequest = JsonConvert.DeserializeObject<PrintRequest>(await Request.Content.ReadAsStringAsync());

            var printproduct = await CommonFunctions.GetPLU<LabelVariables>(plu.ToString());

            if (printRequest.Price != null) { printproduct.Price = printRequest.Price; }
            
            var labelfile = (printRequest.FormatName != null) ? MerchandiserFunctions.FormatCheck(printRequest.FormatName) : MerchandiserFunctions.FormatCheck(Path.GetFileNameWithoutExtension(printproduct.LblName));
            NiceLabel2019.LabelPath = @"C:\Program Files\MM_Label\Labels\" + labelfile;

            NiceLabel2019.SelectedPrinterName = (printRequest.SelectedPrinterName != null) ? printRequest.SelectedPrinterName : File.ReadLines(@"C:\Program Files\MM_Label\System\printer.ini").First();

            NiceLabel2019.LabelQTY = (printRequest.LabelQTY > 1) ? printRequest.LabelQTY : 1;

            NiceLabel2019.SetAllLabelVariables(printproduct);

            var result = NiceLabel2019.PrintLabelAsync();

            return result;
        }
    }
}
