﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Web.Http;
using TheMerchandiserAPI.Models;
using TheMerchandiserAPI.Services;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Windows;
using System.Threading.Tasks;
using TheMerchandiserAPI.Helpers;

namespace TheMerchandiserAPI
{
#if DEBUG

    public class TestController : ApiController
    {

        [Route("sendmsg")][HttpPost]
        public async Task<object> sendmsg()
        {
            var message = await Request.Content.ReadAsStringAsync();

            Process cmd = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = @"C:\Windows\System32\msg.exe",
                    Arguments = @"* " + message,
                    WorkingDirectory = Environment.SystemDirectory
                }
            };

            cmd.Start();
            
            return "msg sent";
        }

        [Route("TestImages/{plu}")]
        [HttpPost]
        public async Task<string> GenerateImages(int plu)
        {
            return await MerchandiserFunctions.GeneratePreviewImages(plu);
        }

    }

#endif
}
