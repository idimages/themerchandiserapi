﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace TheMerchandiserAPI
{
    public class MotdController : ApiController
    {
        
        public object Get()
        {
            Console.WriteLine("Motd Get Request"); 
            
            string result = "Today's Merchandiser news is that it is awesome.";
            
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new StringContent(result, Encoding.UTF8, "application/json");

            return resp;
        
        }

    }
}
