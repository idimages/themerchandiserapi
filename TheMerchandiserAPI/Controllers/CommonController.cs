﻿using Newtonsoft.Json;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using TheMerchandiserAPI.Helpers;
using TheMerchandiserAPI.Models;
using TheMerchandiserAPI.Services;

namespace TheMerchandiserAPI.Controllers
{
    public class CommonController: ApiController
    {
        [Route("getlogoimage")][HttpGet]
        public string GetImage()
        {
            string filepath = @"C:\Program Files\MM_Label\Images\logo.png";
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(filepath));
        }

        [Route("getPrinters")][HttpGet]
        public object GetPrinters()
        {
            var NiceLabel2019 = new LabelSoftwareSDK();
            return NiceLabel2019.PrinterNames;
        }

        /// <summary>
        /// Retrieves a list of installed label formats from a specified directory.
        /// </summary>
        /// <returns>An array of objects containing the name and last modified date of each label format.</returns>
        [Route("getInstalledFormats")][HttpGet]
        public object GetInstalledFormats()
        {
            var Formats = new DirectoryInfo(@"C:\Program Files\MM_Label\Labels")
            .GetFiles()
            .Where(f => f.Extension == ".lbl" || f.Extension == ".nlbl")
            .Select(f => new {
                Name = Path.GetFileNameWithoutExtension(f.Name),
                LastModified = f.LastWriteTime.ToString("M/d/yyyy hh:mmtt")
            })
            .ToArray();
            return Formats;
        }

        [Route("qrcode")][HttpGet]
        public object GenerateQRCode()
        {                   
            var port = MerchandiserFunctions.ReadOptionFromIni("APIPort", "8080");

            var _merchandiserDetails = new MerchandiserDetails()
            {
                Name = MerchandiserFunctions.ReadOptionFromIni("MerchandiserName", "Merchandiser Name Not Configured"),
                IPAddress = CommonFunctions.GetLocalIP(),
                Port = port
            };

            var json = JsonConvert.SerializeObject(_merchandiserDetails);

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(json, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            qrCodeImage.Save(@"C:\Program Files\MM_Label\Images\APIQR.png");

            string result = "QR Code Successfully Generated for IP: " + CommonFunctions.GetLocalIP() + ":" + port;
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new StringContent(result, Encoding.UTF8, "plain/text");
            return resp;
        }

        [Route("SetDeviceInfo")][HttpPost]
        public void PostDeviceInfo(DeviceInfo deviceInfo)
        {
            MerchandiserFunctions.SetOptionToIni("MerchandiserName", deviceInfo.deviceName);
            MerchandiserFunctions.SetOptionToIni("MerchandiserID", deviceInfo.deviceNumber.ToString());
        }

        [Route("GetDeviceInfo")][HttpGet]
        public DeviceInfo GetDeviceInfo()
        {
            return new DeviceInfo(); 
        }

        [Route("SetSetting/{inifile}/{setting}")][HttpPost]
        public void SetSetting(string inifile, string setting)
        {
            if (string.IsNullOrEmpty(setting)) { setting = ""; }
            MerchandiserFunctions.SetOptionToIni(inifile, setting);
        }

        [Route("SetSetting/{inifile}")][HttpPost]
        public void ClearSetting(string inifile)
        {
            MerchandiserFunctions.SetOptionToIni(inifile, "");
        }

        [Route("GetSetting/{inifile}")][HttpGet]
        public string GetSetting(string inifile)
        {
            return MerchandiserFunctions.ReadOptionFromIni(inifile);
        }

        [Route("updateformat/{formatName}")][HttpPost]
        public async Task<IHttpActionResult> Uploadformat(string formatName)
        {
            // Get the binary data from the request body.
            var byteArray = await Request.Content.ReadAsByteArrayAsync();

            // Save the file to the local hard drive.
            var filePath = Path.Combine("C:\\Files", formatName);
            File.WriteAllBytes(filePath, byteArray);

            return Ok(formatName + " installed");
        }
    }
}
