﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using TheMerchandiserAPI.Helpers;
using TheMerchandiserAPI.Models;
using TheMerchandiserAPI.Services;
using Newtonsoft.Json;
using System.IO;

namespace TheMerchandiserAPI.Controllers
{
    [RoutePrefix("api")]
    public class LoggingController : ApiController
    {
        [Route("logging/{searchid}")][HttpGet]
        public async Task<LoggingRequest> Get(string searchid)
        {
            var _residentDB = new MSAccessConnector(@"C:\Program Files\MM_Label\system\printlog.mdb");
            DataTable _searchDataTable;
            int numericplu;

            if (int.TryParse(searchid, out numericplu))
            {
                _searchDataTable = await _residentDB.GetDataTable("SELECT * FROM [PrintHistory] WHERE PLU='" + numericplu + "';");
            }
            else
            {
                _searchDataTable = await _residentDB.GetDataTable("SELECT * FROM [PrintHistory] WHERE [ProductDescription] LIKE '%" + searchid + "%';");
            }

            _searchDataTable.TableName = "SearchResults";

            List<PrintRecord> labelsPrinted = _searchDataTable.AsEnumerable()
                .Select(row => new PrintRecord
                {
                    PLU = row.Field<string>("PLU"),
                    ProductDescription = row.Field<string>("ProductDescription"),
                    Price = row.Field<string>("Price"),
                    ItemType = row.Field<string>("ItemType"),
                    PrintDate = row.Field<string>("PrintDate"),
                    PrintTime = row.Field<string>("PrintTime"),
                    LabelBarcode = row.Field<string>("LabelBarcode"),
                    LabelName = row.Field<string>("LabelName"),
                    PrintForm = row.Field<string>("PrintForm"),
                    Weight = row.Field<string>("Weight"),
                    PricePerPound = row.Field<string>("PricePerPound"),
                    LabelsPrinted = row.Field<string>("LabelsPrinted"),
                    OverrideSig = row.Field<string>("OverrideSig")
                }).ToList();

            return new LoggingRequest() { deviceInfo = new DeviceInfo(), LabelsPrinted = labelsPrinted };
        }

        [Route("logging")][HttpGet]
        public async Task<LoggingRequest> Get()
        {
            var _residentDB = new MSAccessConnector(@"C:\Program Files\MM_Label\system\printlog.mdb");
            DataTable _searchDataTable;
            
            _searchDataTable = await _residentDB.GetDataTable("SELECT * FROM [PrintHistory];");

            _searchDataTable.TableName = "SearchResults";

            List<PrintRecord> labelsPrinted = _searchDataTable.AsEnumerable()
                .Select(row => new PrintRecord
                {
                        PLU = row.Field<string>("PLU"),
                        ProductDescription = row.Field<string>("ProductDescription"),
                        Price = row.Field<string>("Price"),
                        ItemType = row.Field<string>("ItemType"),
                        PrintDate = row.Field<string>("PrintDate"),
                        PrintTime = row.Field<string>("PrintTime"),
                        LabelBarcode = row.Field<string>("LabelBarcode"),
                        LabelName = row.Field<string>("LabelName"),
                        PrintForm = row.Field<string>("PrintForm"),
                        Weight = row.Field<string>("Weight"),
                        PricePerPound = row.Field<string>("PricePerPound"),
                        LabelsPrinted = row.Field<string>("LabelsPrinted"),
                        OverrideSig = row.Field<string>("OverrideSig")
                }).ToList();

            return new LoggingRequest() { deviceInfo = new DeviceInfo(), LabelsPrinted=labelsPrinted };
        }

    }
}
