﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMerchandiserAPI.Services
{
    public class MSAccessConnector
    {
        private string _connstring;

        public MSAccessConnector(string mdbfile)
        {
            _connstring = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + mdbfile + ";Persist Security Info=False;";
        }

        public async Task<DataTable> GetDataTable(string sql)
        {
                using (var conn = new OleDbConnection(_connstring))
                {
                    await conn.OpenAsync();
                    var cmd = new OleDbCommand(sql, conn);                    
                    var datareader = await cmd.ExecuteReaderAsync();
                    DataTable dt = new DataTable();
                    dt.Load(datareader);

                    foreach (DataColumn col in dt.Columns)
                    {
                        col.ColumnName = col.ColumnName.Replace(" ", "");

                    }

                    return dt;
                }
        }
    }
}
