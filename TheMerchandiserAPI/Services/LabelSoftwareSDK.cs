﻿using NiceLabel.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMerchandiserAPI.Models;

namespace TheMerchandiserAPI.Services
{
    public class LabelSoftwareSDK
    {
        //Variables
        private byte[] _preview;
        private IPrintEngine _printEngine;

        public LabelSoftwareSDK()
        {
            _printEngine = PrintEngineFactory.PrintEngine;
            InitializePrintEngine();
            GeneratePrinterNameList();
        }
        
        //Print Related Properties
        private IList<IPrinter> _printers;
        public IList<IPrinter> Printers
        {
            get
            {
                if (_printers == null)
                {
                    _printers = _printEngine.Printers;

                    if (_printers.Count > 0)
                    {
                        SelectedPrinter = _printers[0];
                        SelectedPrinterName = _printers[0].Name;
                    }
                }

                return _printers;
            }
        }
        private IList<string> _printernames = new List<string>();
        public IList<string> PrinterNames { get { return _printernames; } private set { _printernames = value; } }
        public string SelectedPrinterName { get; set; }
        public IPrinter SelectedPrinter { get; private set; }
        private int _labelqty = 1;
        public int LabelQTY { get { return _labelqty; } set { _labelqty = value; } }

        //Label Related Properties
        private string _labelpath;
        public string LabelPath
        {
            get { return _labelpath; }
            set { _labelpath = value; Label = _printEngine.OpenLabel(_labelpath); }
        }
        public ILabel Label { get; set; }
        public IList<IVariable> Variables
        {
            get
            {
                if (Label == null)
                {
                    return null;
                }

                return Label.Variables;
            }
        }

        //Methods
        private void GeneratePrinterNameList()
        {
            foreach (var printer in Printers)
            {
                _printernames.Add(printer.Name);
            }
        }

        public string PrintLabel()
        {
            try
            {
                Label.PrintSettings.PrinterName = SelectedPrinterName;
                Label.Print(LabelQTY);
                return string.Empty;

            }
            catch (SDKException ex)
            {
                return ex.DetailedMessage;
            }
        }

        public string PrintLabelAsync()
        {
            try
            {
                Label.PrintSettings.PrinterName = SelectedPrinterName;
                Label.PrintAsync(LabelQTY);
                return "Sent " + LabelQTY.ToString() + " Label(s) to " + SelectedPrinterName;

            }
            catch (SDKException ex)
            {
                return ex.DetailedMessage;
            }
        }

        private void InitializePrintEngine()
        {
            try
            {
                _printEngine.Initialize();
            }
            catch (SDKException exception)
            {
                Console.WriteLine("Initialization of the NiceLabel SDK failed." + Environment.NewLine + Environment.NewLine + exception.ToString());
            }
        }

        public void ShutDownPrintEngine()
        {
            _printEngine.Shutdown();
        }

        public byte[] GeneratePreviewImage()
        {

            if (Label != null)
            {
                try
                {
                    ILabelPreviewSettings settings = new LabelPreviewSettings();
                    settings.PreviewToFile = false;
                    settings.ImageFormat = "png";
                    settings.UseDefaultSize = true;
                    //settings.Width = 500;
                    settings.ShowSampleValues = false;

                    _preview = (byte[])Label.GetLabelPreview(settings);
                }
                catch (SDKException ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            return _preview;
        }

        public void SetAllLabelVariables(LabelVariables labelVariables)
        {
            string varVal;

            foreach (var variable in Variables)
            {
                if (labelVariables.GetType().GetProperty(variable.Name.ToString()) != null)
                {
                    varVal = (labelVariables.GetType().GetProperty(variable.Name.ToString())?.GetValue(labelVariables, null)?.ToString());

                }
                else
                {
                    varVal = "";
                }

                SetVariable(variable.Name.ToString(), varVal);

            }

            GeneratePreviewImage();
        }

        public void SetVariable(string variableName, string value)
        {
            Label.Variables[variableName].SetValue(value);
        }

    }
}
