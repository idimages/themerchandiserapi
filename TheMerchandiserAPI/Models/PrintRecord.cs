﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMerchandiserAPI.Models
{
    public class PrintRecord
    {
        public string PLU { get; set; }
        public string ProductDescription { get; set; }
        public string Price { get; set; }
        public string PricePerPound { get; set; }
        public string ItemType { get; set; }
        public string PrintDate { get; set; }
        public string PrintTime { get; set; }
        public string LabelBarcode { get; set; }
        public string LabelName { get; set; }
        public string PrintForm { get; set; }
        public string Weight { get; set; }
        public string LabelsPrinted { get; set; }
        public string OverrideSig { get; set; }
    }
}
