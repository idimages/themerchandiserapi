﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMerchandiserAPI.Models
{
    public class Product
    {
        public object ProdID { get; set; }
        public string PLU { get; set; }
        public string Price { get; set; }
        public string SalePrice { get; set; }
        public string ZonePrice1 { get; set; }
        public string ZonePrice2 { get; set; }
        public string ZonePrice3 { get; set; }
        public string ZonePrice4 { get; set; }
        public string ZonePrice5 { get; set; }
        public string ZonePrice6 { get; set; }
        public string ZonePrice7 { get; set; }
        public string ZonePrice8 { get; set; }
        public string ZonePrice9 { get; set; }
        public string ZonePrice10 { get; set; }
        public string ZonePrice11 { get; set; }
        public string ZonePrice12 { get; set; }
        public string ZonePrice13 { get; set; }
        public string ZonePrice14 { get; set; }
        public string ZonePrice15 { get; set; }
        public string ZonePrice16 { get; set; }
        public string ZonePrice17 { get; set; }
        public string ZonePrice18 { get; set; }
        public string ZonePrice19 { get; set; }
        public string ZonePrice20 { get; set; }
        public string CategoryNum { get; set; }
        public string ShelfLife { get; set; }
        public string ShelfLifeType { get; set; }
        public string Barcode { get; set; }
        public string NetWt { get; set; }
        public string BarType { get; set; }
        public string VID1 { get; set; }
        public bool PricePerLbEn { get; set; }
        public bool DateEn { get; set; }
        public bool QtyEn { get; set; }
        public bool Scaleable { get; set; }
        public string Tare { get; set; }
        public bool StoreIDEn { get; set; }
        public bool NutriFactEn { get; set; }
        public bool NewItemFlg { get; set; }
        public string Button1 { get; set; }
        public string Button2 { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string Description4 { get; set; }
        public string Description5 { get; set; }
        public string Description6 { get; set; }
        public string Description7 { get; set; }
        public string Description8 { get; set; }
        public string Description9 { get; set; }
        public string Description10 { get; set; }
        public string Description11 { get; set; }
        public string Description12 { get; set; }
        public string Description13 { get; set; }
        public string Description14 { get; set; }
        public string IngredientNum { get; set; }
        public string Ingredients { get; set; }
        
        public DateTime SaleStart { get; set; }
        
        public DateTime SaleEnd { get; set; }
        
        public string LblName { get; set; }
        public string LblName2 { get; set; }
        public string LblName3 { get; set; }
        public string LblName4 { get; set; }
        public string LblName5 { get; set; }
        public string NutrifactNum { get; set; }
        public string NFServingSize { get; set; }
        public string NFCalories { get; set; }
        public string NFCaloriesFromFat { get; set; }
        public string NFTotalFat { get; set; }
        public string NFTotalFatG { get; set; }
        public string NFSatFatG { get; set; }
        public string NFCholesterol { get; set; }
        public string NFCholesterolMG { get; set; }
        public string NFSodium { get; set; }
        public string NFSodiumMG { get; set; }
        public string NFTotCarbo { get; set; }
        public string NFTotCarboG { get; set; }
        public string NFDietFiber { get; set; }
        public string NFSugars { get; set; }
        public string NFProtein { get; set; }
        public string NFVitA { get; set; }
        public string NFVitC { get; set; }
        public string NFCalcium { get; set; }
        public string NFIron { get; set; }
        public string NF1 { get; set; }
        public string NF2 { get; set; }
        public string NF3 { get; set; }
        public string NF4 { get; set; }
        public string NF5 { get; set; }
        public string NF6 { get; set; }
        public string NF7 { get; set; }
        public string NF8 { get; set; }
        public string NF9 { get; set; }
        public string NF10 { get; set; }
        public string NFDesc { get; set; }
        public string NFSugarsAdded { get; set; }
        public string NFSugarsAddedG { get; set; }
        public string NFVitD { get; set; }
        public string NFVitDmcg { get; set; }
        public string NFCalciummcg { get; set; }
        public string NFIronmcg { get; set; }
        public string NFPotassium { get; set; }
        public string NFPotassiummcg { get; set; }
        public string NF11 { get; set; }
        public string NF12 { get; set; }
        public string NF13 { get; set; }
        public string NF14 { get; set; }
        public string NF15 { get; set; }
        public string NF16 { get; set; }
        public string NF17 { get; set; }
        public string NF18 { get; set; }
        public string NF19 { get; set; }
        public string NF20 { get; set; }
        public string Description15 { get; set; }
        public string Description16 { get; set; }
        public string Dept { get; set; }
        public string PageNum { get; set; }

    }
}
