﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMerchandiserAPI.Models
{
    public class PrintRequest
    {
        //Check for valid Print Request
        public string ApiKey { get; set; }
        
        //Print Parameters
        public string SelectedPrinterName { get; set; }
        public int LabelQTY { get; set; }
        public string FormatName { get; set; }

        //Override Variables
        public string Price { get; set; }
        public string UnityQty { get; set; }
    }
}
