﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace TheMerchandiserAPI.Models
{
    public class LoggingRequest
    {
        public DeviceInfo deviceInfo { get; set; }
        public List<PrintRecord> LabelsPrinted { get; set; }

    }
}
