﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMerchandiserAPI.Models
{
    class MerchandiserDetails
    {
        public string Name { get; set; }
        public string IPAddress { get; set; }
        public string Port { get; set; }
    }
}

