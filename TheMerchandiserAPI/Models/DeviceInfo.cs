﻿using System;
using TheMerchandiserAPI.Helpers;

namespace TheMerchandiserAPI.Models
{
    public class DeviceInfo
    {
        public DeviceInfo() 
        {
            deviceName = MerchandiserFunctions.ReadOptionFromIni("MerchandiserName", "Name not Set");
            deviceNumber = MerchandiserFunctions.ReadOptionFromIni("MerchandiserID", "ID not Set");
            deviceDate = DateTime.Now.ToShortDateString();
            deviceTime = DateTime.Now.ToString("HH:mm:ss");
            deviceIPAddress = CommonFunctions.GetLocalIP();
        }
        public string deviceName { get; set; }
        public string deviceNumber { get; set; }
        public string deviceDate { get; set; }
        public string deviceTime { get; set; }
        public string deviceIPAddress { get; set; }

    }
}
