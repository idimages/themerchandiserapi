﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMerchandiserAPI.Helpers;

namespace TheMerchandiserAPI.Models
{
    public class LabelVariables : Product
    {
        //Constructor
        BarcodeLogic bc;
        public LabelVariables() { bc = new BarcodeLogic(); }

        // Properties
        public string BarcodeType { get { return GetBarcodeType(); } }
        public string BarcodeVal { get { return GenerateBarcodeVal(); } }
        public string BarcodeValEAN13 { get { return GenerateEAN13BarcodeVal(); } }
        public string ButtonLine1 { get { return Button1; } }
        public string ButtonLine2 { get { return Button2; } }
        public string ConnLicNumber { get { return MerchandiserFunctions.ReadOptionFromIni("ConnLicNum"); } }       
        public string DBPrice { get { return Price; } }
        public string DBBarcodeVal { get { return Barcode; } }
        public string DBWeight { get { return NetWt; } }
        public string LabelFile { get { return LblName; } }
        public string NFCaloriesFat { get { return NFCaloriesFromFat; } }
        public string NFSugarsG { get { return NFSugars; } }
        public string NFProteinG { get { return NFProtein; } }
        public string NFServingPerPack { get { return NFDesc; } }
        public string NFSatFat { get { return NF1; } }
        public string NFDietFiberG { get { return NF2; } }
        public string NFThiamin { get { return NF3; } }
        public string NFRiboflavin { get { return NF4; } }
        public string NFNiacin { get { return NF5; } }
        public string NFFolicAcid { get { return NF6; } }
        public string NFSugarsAlcG { get { return NF7; } }
        public string NFTransFatG { get { return NF8; } }
        //need to populate
        public string PricePerPound { get { return string.Empty; } }
        public string QTYEnabled { get { return QtyEn.ToString(); } }
        //need to populate
        public string RawWeight { get { return string.Empty; } }
        public string SaleDateStart { get { return SaleStart.ToString("d"); } }
        public string SaleDateEnd { get { return SaleEnd.ToString("d"); } }
        public string SellByDays { get { return ShelfLife; } }
        public string SellByDate { get { return DateTime.Today.AddDays(Double.Parse(ShelfLife)).ToString("d"); } }
        public string StoreAddress { get { return MerchandiserFunctions.ReadOptionFromIni("StoreAddress"); } }
        public string StoreNum { get { return MerchandiserFunctions.ReadOptionFromIni("StoreID"); } }
        public string TempLabelFile { get { return LblName; } }        
        //need to populate
        public string UnitQty { get { return string.Empty; } }
        public string VendorID { get { return GetVendorID(); } }
        //need to populate
        public string Weight { get { return string.Empty; } }
        public string WeightFromScale { get { return string.Empty; } }

        // Methods        
        private string GetBarcodeType()
        {
            return (int.Parse(BarType) > 1) ? (double.Parse(Price) > 99.99) ? "0" : BarType : BarType;
        }
        private string GenerateBarcodeVal()
        {
            var _tempPrice = MerchandiserFunctions.PriceToDouble(Price);

            if (_tempPrice <= 0.01) { Price = string.Empty; return string.Empty; }

            switch (int.Parse(BarcodeType))
            {
                case 0:
                    return bc.FormatTOT1Barcode(bc.PaddedBarcode(Barcode), bc.BarcodePriceVal(bc.BarcodePaddedPrice(Price), 5));
                case 1:
                    return bc.FormatTOT2Barcode(bc.PaddedBarcode(Barcode), bc.BarcodePriceVal(bc.BarcodePaddedPrice(Price), 4));
                default:
                    return string.Empty;
            }
        }
        private string GenerateEAN13BarcodeVal()
        {
            if (MerchandiserFunctions.ReadOptionFromIni("EAN13") == "YES")
            {
                return bc.FormatEAN13Barcode(Barcode, bc.BarcodePriceVal(Price, 5));
            }

            return string.Empty;
        }
        private string GetVendorID()
        {
            return (BarcodeType == "2") ? VID1 : MerchandiserFunctions.ReadOptionFromIni("VID1Value");
        }              
        
    }
}
